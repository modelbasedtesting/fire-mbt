
How do I use this?
==================

1. Clone the repo into a folder (e.g. called fire-mbt)
2. In IntelliJ, choose "Open Project" and select the fire-mbt (or your folder name) folder
3. Project opens
4. Run the tests by right-clicking the fire-mbt/src/test/java/TestFire.java in
   the project panel and selecting "Run", or using the existing "Fire Tests" run
   configuration.



