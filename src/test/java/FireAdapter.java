import java.io.File;

public class FireAdapter {
    private Fire fire = new Fire();
    private User user;
    private Lab lab = new Lab();

    public void login() {
        user = fire.login();
    }

    public void logout() {
        fire.logout();
    }

    public void clickLab() {
        user.selectLab(lab);
    }

    public void addFile() {
        user.getLab().addFile(new File("/tmp/foo.txt"));
    }

    public void rmFile() {
        File firstFileInList = user.getLab().getFiles().get(0);
        user.getLab().rmFile(firstFileInList);
    }

    public void submit() {
        user.getLab().submit();
    }

    public void withdraw() {
        user.getLab().withdraw();
    }
}
