import nz.ac.waikato.modeljunit.Action;
import nz.ac.waikato.modeljunit.FsmModel;

public class FireModel implements FsmModel {
    private FireAdapter adapter = new FireAdapter();
    private LabState s = LabState.Open;
    private int f = 0;

    private enum LabState {Open, Submitted}

    private enum State {Login, Home, Lab}

    private State state = State.Login;

    public Object getState() {
        return state;
    }

    public void reset(boolean b) {
        state = State.Login;
        f = 0;
        s = LabState.Open;
    }

    @Action
    public void login() {
        if (state == State.Login) {
            adapter.login();
            state = State.Home;
        }
    }

    @Action
    public void logout() {
        if (state != State.Login) {
            adapter.logout();
            state = State.Login;
        }
    }

    @Action
    public void clickLab() {
        if (state == State.Home) {
            adapter.clickLab();
            state = State.Lab;
        }
    }

    @Action
    public void addFile() {
        if (state == State.Lab && s == LabState.Open) {
            adapter.addFile();
            f++;
        }
    }

    @Action
    public void rmFile() {
        if (state == State.Lab && s == LabState.Open && f > 0) {
            adapter.rmFile();
            f--;
        }
    }

    @Action
    public void submit() {
        if (state == State.Lab && s == LabState.Open) {
            adapter.submit();
            s = LabState.Submitted;
        }
    }

    @Action
    public void withdraw() {
        if (state == State.Lab && s == LabState.Submitted) {
            adapter.withdraw();
            s = LabState.Open;
            f = 0;
        }
    }
}
