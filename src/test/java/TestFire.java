import nz.ac.waikato.modeljunit.RandomTester;
import nz.ac.waikato.modeljunit.StopOnFailureListener;
import nz.ac.waikato.modeljunit.Tester;
import nz.ac.waikato.modeljunit.VerboseListener;
import nz.ac.waikato.modeljunit.coverage.ActionCoverage;
import nz.ac.waikato.modeljunit.coverage.StateCoverage;
import nz.ac.waikato.modeljunit.coverage.TransitionCoverage;
import org.junit.Test;

public class TestFire {
    @Test
    public void testFire() throws Exception {
        FireModel fireModel = new FireModel();
        Tester tester = new RandomTester(fireModel);

        tester.buildGraph();
        tester.addListener(new VerboseListener());
        tester.addListener(new StopOnFailureListener());
        tester.addCoverageMetric(new TransitionCoverage());
        tester.addCoverageMetric(new StateCoverage());
        tester.addCoverageMetric(new ActionCoverage());

        tester.generate(200000);
        tester.printCoverage();
    }

    @Test
    public void regressionTest1() throws Exception {
        FireModel model = new FireModel();

        model.login();
        model.clickLab();
        model.submit();
        model.withdraw();
        model.addFile(); // should crash
    }

    @Test
    public void testRegression2() throws Exception {
        FireModel model = new FireModel();

        model.login();
        model.clickLab();
        model.addFile();
        model.logout();

        model.login();
        model.clickLab();
        model.rmFile(); // Crashes
    }
}
